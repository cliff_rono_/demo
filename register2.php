<?php
ob_start();
session_start();

if(isset($_POST['name'])){
    $name = $_POST['name'];
    if($name != '' && !ctype_space($name)){
      $name = preg_replace('/[^a-zA-Z0-9_-]/','',$name);  
    }else{
        die('Kindly fill your name');
        
    }
    
}
if(isset($_POST['email'])){
    $email = $_POST['email'];
    if(!empty($email) && !ctype_space($email)){
        if(filter_var($email,FILTER_VALIDATE_EMAIL)){
      $email = filter_var($email,FILTER_SANITIZE_EMAIL);  
    }else{
        die('Invalid Email Address Provided');
        
    }
    }else{
        die('Email address is required');
    }
    
}
if(isset($_POST['phone'])){
    $phone = $_POST['phone'];
    if($phone != '' && !ctype_space($phone)){
      $phone = intval($phone);
        if(!$phone){
            die("Invalid Phone Number");
        }
    }else{
        die('Kindly fill your name');
        
    }
    
}
if(isset($_POST['password'])){
    $password = $_POST['password'];
}
   if(isset($_POST['password2'])){
       $password2 = $_POST['password2'];
   }
   
   if($password !== $password2){
       die("Passwords Do Not Match!");
   }else{
       $password = password_hash($password,PASSWORD_BCRYPT,array("cost" => 12));
   }
  $statement = "INSERT INTO user(name,email,phone,password) VALUES(?,?,?,?)";
   $stmt = $db->prepare($statement);
   if($stmt->execute([$name,$email,$phone,$password])){
       echo "Registration Successful";
   }else{
       echo "System Error, Kindly Contact Administrator";
   }
   
   