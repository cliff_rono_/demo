<!--
Author: /
-->

<!DOCTYPE html>
<html>
<head>

	<title>PORAEH</title>

	<!-- For-Mobile-Apps -->
		<meta name="viewport" content="width=device-width, initial-scale=1" />
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		<meta name="keywords" content="Classy Forms Widget Responsive, Login Form Web Template, Flat Pricing Tables, Flat Drop-Downs, Sign-Up Web Templates, Flat Web Templates, Login Sign-up Responsive Web Template, Smartphone Compatible Web Template, Free Web Designs for Nokia, Samsung, LG, Sony Ericsson, Motorola Web Design" />
		<script type="application/x-javascript"> addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false); function hideURLbar(){ window.scrollTo(0,1); } </script>
	<!-- //For-Mobile-Apps -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
	<!-- Style --> <link rel="stylesheet" href="assets/css/style.css" type="text/css" media="all" />
	<!-- Default-JavaScript-File --> <script type="text/javascript" src="assets/js/jquery.min.js"></script>

	<!-- Web-Fonts -->
		<link href='//fonts.googleapis.com/css?family=Raleway:400,500,600,700,800' rel='stylesheet' type='text/css'>
		<link href='//fonts.googleapis.com/css?family=Open+Sans:400,600,700' rel='stylesheet' type='text/css'>
	<!-- //Web-Fonts -->
<style>
    h2.error{
        color:red;
        text-align: center;
    } 
    #requestReset{
        background:transparent;
        border:2px solid #008CBA;
        padding:10px 25px;
        color:#008CBA;
        margin-bottom: 10px;
    }
</style>
</head>
<body>

	<h1>Register with us Today!</h1>
    <h2 class="error"></h2>
	<div class="container">



		<div class="tab">

			<div id="horizontalTab" style="display: block; width: 100%; margin: 0px;">
				<script src="assets/js/easyResponsiveTabs.js" type="text/javascript"></script>
				<script type="text/javascript">
					$(document).ready(function () {
						$('#horizontalTab').easyResponsiveTabs({
							type: 'default', //Types: default, vertical, accordion
							width: 'auto', //auto or any width like 600px
							fit: true,   // 100% fit in a container
							closed: 'accordion', // Start closed if in accordion view
							activate: function(event) { // Callback function if tab is switched
								var $tab = $(this);
								var $info = $('#tabInfo');
								var $name = $('span', $info);
								$name.text($tab.text());
								$info.show();
							}
						});

						$('#verticalTab').easyResponsiveTabs({
							type: 'vertical',
							width: 'auto',
							fit: true
						});
					});
				</script>

				<div class="tabs">

					<div class="tab-left">
						<ul class="resp-tabs-list">
							<li class="resp-tab-item">User Registration</li>
							<li class="resp-tab-item">Login</li>
							<li class="resp-tab-item">More Info</li>
						</ul>
					</div>

					<div class="tab-right">
						<div class="resp-tabs-container">
							<div class="tab-1 resp-tab-content">
								<div class="w3l-sign-in">
									<h3>User Registration</h3>
									<form action="" method="post" id="registration_form">
										<input type="text" class="name" name="fname" placeholder="First Name" required="">
										<input type="text" class="name" name="mname" placeholder="Middle Name" required="">
										<input type="text" class="name" name="lname" placeholder="Last Name" required="">
										<input type="email" class="email" name="register_email" placeholder="Email" required="">
										<input type="text" class="email" name="phone" placeholder="phone" required="">
										<input type="password" class="password" name="password" placeholder="Password" required="">
										<input type="password" class="password" name="password2" placeholder="Confirm Password" required="">
										<ul class="w3agile">
											<li>
												<input type="checkbox" id="brand1" value="">
												<label for="brand1"><span></span>Remember me</label>
											</li>
										</ul>
										<input type="submit" value="Register" name="register" id="regBtn">
										<div class="clear"></div>
									</form>
								</div>
							</div>
							<div class="tab-1 resp-tab-content">
								<div class="agileinfo-recover">
								<h3>User Login</h3>
									<form action="" method="post" id="loginForm">
									<select name="user">
											<option disabled selected>LOGIN AS..</option>
											<option  value="user">User</option>
											<option  value="agent">Agents</option>
											<option  value="superagent">Super Agent</option>
											<option  value="merchant">Merchant</option>
										</select>

										<br><br>
										<input type="text" class="email" name="email_login" placeholder="Email" required="">
										<input type="password" class="password" name="password" placeholder="Password" required="">
										
										<input type="submit" class="send" name="login" value="Login" id="logBtn">
										
									</form>
									<br>
									<div class="text-center"><a  style="color:#ffffff;" href="#forgot" data-toggle="modal" data-target="#forgot">Forgot Password?</a></div>
								</div>
							</div>
							<div class="tab-1 resp-tab-content">
								<div class="agile-send-mail">
								<h3>More Info</h3>
								<p>
									An agent is the owner of a Pikash outlet who can perform services of Money Transfer,
									 Paybill, Bus and Airline ticketing, loading e-wallets, Insurance premium collection and Agency Banking.
								</p>
								<br>
								<p>
									A super-agent is one who manages 25 Agents within or outside the same region.
								</p>
								<br>
								<p>
									A merchant is one who owns a retail outlet
									and uses the Pikash platform that the Point-of-Sale for his customers to purchase goods.
								</p>
							</div>
							</div>
						</div>
					</div>

					<div class="clear"></div>

				</div>

			</div>

		</div>

	</div>
<div class="modal fade" id="forgot" role="dialog">
    <div class="modal-dialog">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Password Reset</h4>
        </div>
        <div class="modal-body">
          <div class="row">
              <div class="col-sm-8 col-sm-offset-2">
                  <input type="email" name="" id="recoveryEmail" class="form-control">
              </div>
          </div>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" id="requestReset">Send Email</button>
        </div>
      </div>
      
    </div>
  </div>
  


	<div class="footer">
		<p> &copy; 2018 All Rights Reserved | <a href="http://poraehinvestment.com" target="_blank">Poraeh</a></p>
	</div>
<script>
    $(document).ready(function(){
        $('#requestReset').click(function(){
            var email = $('#recoveryEmail').val();
            if($.trim(email) != ''){
                $.ajax({
                    type: 'POST',
                    url: 'form-processing/',
                    data: {
                        'reset_email':email
                    },
                    success: function(response){
                        
                    }
                });
            }
        });
        $('#registration_form').submit(function(evt){
            evt.preventDefault();
            $('#regBtn').val('Processing...').prop('disabled',true);
            var data = $(this).serialize();
            var url = 'form-processing/';
                $.ajax({
                    type:'POST',
                    url:url,
                    data,data,
                    success: function(response){
                      $('#regBtn').val('Register').prop('disabled',false);
                        if(response.indexOf('Successful') > -1){
                            
                            location.href = "user/";
                        }else{
                            $('.error').hide().text(response).fadeIn(800);
                        }
                    }
                });
        });
        $('#loginForm').submit(function(evt){
            evt.preventDefault();
            $('#logBtn').val('Processing...').prop('disabled',true);
            var data = $(this).serialize();
            var url = 'form-processing/';
                $.ajax({
                    type:'POST',
                    url:url,
                    data,data,
                    success: function(response){
                      $('#logBtn').val('Login').prop('disabled',false);
                        if(response.indexOf('Successful') > -1){
                            
                            location.href = "user/";
                        }else{
                            $('.error').hide().text(response).fadeIn(800);
                        }
                    }
                });
        });
    });
    </script>

  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
</body>
</html>
