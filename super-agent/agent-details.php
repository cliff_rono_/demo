
<!DOCTYPE html>
<html lang="zxx">
<head>
<title></title>
<!-- custom-theme -->
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="keywords" content="" />
<script type="application/x-javascript"> addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false);
		function hideURLbar(){ window.scrollTo(0,1); } </script>

<link href="../css/bootstrap.css" rel="stylesheet" type="text/css" media="all" />
<link href="../css/component.css" rel="stylesheet" type="text/css" media="all" />
<link href="../css/style_grid.css" rel="stylesheet" type="text/css" media="all" />
<link href="../css/style.css" rel="stylesheet" type="text/css" media="all" />
<!-- font-awesome-icons -->
<link href="../css/font-awesome.css" rel="stylesheet"> 
<!-- //font-awesome-icons -->
<link href="//fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i,800" rel="stylesheet">
<style>
    div.agent-details button{
        margin-right:15px;
        background:transparent;
        border:2px solid #008CBA;
        margin-bottom: 5px;
        color:#008CBA;
    }    
    div.agent-details button:hover{
        
    }
    .nav-tabs{
        border-bottom: 2px solid #008CBA;
    }
    .nav-tabs > li.active > a, .nav-tabs > li.active > a:hover, .nav-tabs > li.active > a:focus{
        border:2px solid #008cba;
        border-bottom-color:#fff;
        margin-bottom:-2px;
    }
</style>
</head>
<body>
<!-- banner -->
<div class="wthree_agile_admin_info">
		  <!-- /w3_agileits_top_nav-->
<?php require_once "snippets/nav.php"; ?>
		<div class="clearfix"></div>
		<!-- //w3_agileits_top_nav-->
		
		<!-- /inner_content-->
				<div class="inner_content">
				    <!-- /inner_content_w3_agile_info-->

					<!-- breadcrumbs -->
						<div class="w3l_agileits_breadcrumbs">
							<div class="w3l_agileits_breadcrumbs_inner">
								<ul>
									<li><a href="index.php">Home</a><span>«</span></li>
									<li>Accounts <span>«</span></li>
									<li>My Agent</li>
								</ul>
							</div>
						</div>
					<!-- //breadcrumbs -->

					<div class="inner_content_w3_agile_info two_in">
					  <div class="agent-details">
                      <!--										Name,location,today transactions,last active,transactions link,current float,-->
					      <button class="agent-info btn">
					          Name: Agent Name
					      </button>
					      <button class="agent-info btn ">Location: Agent Location</button>
					      <button class="agent-info btn">Last Active: 12-10-2018 13:56:32</button>
					      <button class="agent-info btn">Current Float: KES 1200.00</button>
					      <a href="transactions.php">Agent Transactions</a>
					      
					  </div>

							<!-- /blank -->
								<div class="blank_w3ls_agile">
									<div class="blank-page agile_info_shadow">
				    <ul class="nav nav-tabs">
                            <li class="active"><a href="#weekly-overview" data-toggle="tab"> <span class="hidden-xs">Weekly</span></a></li>
                            <li><a href="#monthly-overview" data-toggle="tab"><span class="hidden-xs"> Monthly</span></a></li>
                            
                           
                        </ul>
                        
                        <div class="tab-content" id="#body-div">
                            
                            <div class="tab-pane active" id="weekly-overview">
                               <div id="weekly_chart_div"></div> 
                            </div>
                            <div class="tab-pane" id="monthly-overview">
                                <div id="monthly_chart_div"></div>
                            </div>
                            
                        </div>
									
									

									</div>
								</div>
							<!-- //blank -->
				    </div>
					<!-- //inner_content_w3_agile_info-->
				</div>
		<!-- //inner_content-->
	</div>
<!-- banner -->
<!--copy rights start here-->
<div class="copyrights">
	 <p>© 2018 Pikash</p>
</div>	
<!--copy rights end here-->
<!-- js -->
          <script type="text/javascript" src="../js/jquery-2.1.4.min.js"></script>
          <script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
          <script src="../js/weekly.js"></script>
          <script src="../js/monthly_charts.js"></script>
		  <script src="../js/modernizr.custom.js"></script>
		   <script src="../js/classie.js"></script>
		  <script src="../js/gnmenu.js"></script>
		  <script>
			new gnMenu( document.getElementById( 'gn-menu' ) );
		 </script>
<!-- //js -->
<script src="../js/screenfull.js"></script>
		<script>
		$(function () {
			$('#supported').text('Supported/allowed: ' + !!screenfull.enabled);

			if (!screenfull.enabled) {
				return false;
			}

			

			$('#toggle').click(function () {
				screenfull.toggle($('#container')[0]);
			});	
		});
		</script>
<script src="../js/jquery.nicescroll.js"></script>
<script src="../js/scripts.js"></script>

<script type="text/javascript" src="../js/bootstrap-3.1.1.min.js"></script>


</body>
</html>