<?php
ob_start();
session_start();
require_once "../config/pik-dbase-config.php";
require_once "../config/pik-functions.php";
require_once "../config/pik_functions_custom.php";
##PROCESS Agent Registration
if(isset($_POST['register_email'])){
   
    if(isset($_POST['name'])){
    $name = $_POST['name'];
    if($name != '' && !ctype_space($name)){
        
      //$name = preg_replace('/[^a-zA-Z0-9_-]/','',$name);  
    }else{
        die('Kindly fill your name');
        
    }
    
}
if(isset($_POST['register_email'])){
    $email = $_POST['register_email'];
    if(!empty($email) && !ctype_space($email)){
        if(filter_var($email,FILTER_VALIDATE_EMAIL)){
      $email = filter_var($email,FILTER_SANITIZE_EMAIL);
            
        if(!POC::getEmailInstance($email,$db)){
            die("Email Address Already Associated With A Pikash Account!");
        }
    }else{
        die('Invalid Email Address Provided');
        
    }
    }else{
        die('Email address is required');
    }
    
}
if(isset($_POST['phone'])){
    $phone = $_POST['phone'];
    if($phone != '' && !ctype_space($phone)){
      $phone = intval($phone);
        $condition = POC::getEmail2Instance($phone,$db);
        if($condition == "default false"){
            die("Phone Number Already Associated With A Pikash Account!");
        }
//       if(!POC::getEmail2Instance($phone,$db)){
//            die("Phone Number Already Associated With A Pikash Account!");
//        }
    }else{
        die('Kindly fill your Phone Number');
        
    }
    
}
    
    
    
if(isset($_POST['location'])){
    $location = $_POST['location'];
}else{
    die("Kindly select Agent's Location");
}
if(isset($_POST['super_agent'])){
    $super_agent = $_POST['super_agent'];
}else{
    $super_agent == "";
}
    
    
    
if(isset($_POST['password'])){
    $password = $_POST['password'];
}
if(isset($_POST['password2'])){
   $password2 = $_POST['password2'];
}  
if($password !== $password2){
   die("Passwords Do Not Match!");
}else{
   $password = password_hash($password,PASSWORD_BCRYPT,array("cost" => 12));
}
    
    
##CREATE A RECORD
$statement = "INSERT INTO agent(name,email,phone,location,super_agent,password) VALUES(?,?,?,?,?,?)";
$stmt = $db->prepare($statement);
if($stmt->execute([$name,$email,$phone,$location,$super_agent,$password])){
   $user_id = $db->lastInsertId();
   $amount = my_encrypt(0,$key);
   $sql_statement = "INSERT INTO agents_pikash_accounts(agent_id,account_balance) VALUES(?,?)";
   $stmt2 = $db->prepare($sql_statement);
   if($stmt2->execute([$user_id,$amount])){
   echo "Registration Successful";  
   }
  }else{
   echo "System Error, Kindly Contact Administrator";
}
   
   
}