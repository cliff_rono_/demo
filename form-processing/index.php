<?php
ob_start();
session_start();
require_once "../config/pik-dbase-config.php";
require_once "../config/pik-functions.php";

//if(isset($_POST['reset_email'])){
//    $email = $_POST['reset_email'];
//    $query = "SELECT * FROM user WHERE email = ?";
//    $stmt = $db->prepare($query);
//    $stmt->execute([$email]);
//    while($row = $stmt->fetch(PDO::FETCH_OBJ)){
//        
//    }
//}

##PROCESS LOGIN
if(isset($_POST['email_login'])){
    $email = $_POST['email_login'];
    $password = $_POST['password'];
    
    $sql = "SELECT password,id FROM user WHERE email = ?";
    $stmt = $db->prepare($sql);
    $stmt->execute([$email]);
    if($stmt->rowCount() < 1){
        die("Invalid email or password");
    }else{
        while($row = $stmt->fetch(PDO::FETCH_OBJ)){
            $db_password = $row->password;
            $user_id = $row->id;
        }
        if(password_verify($password,$db_password)){
           $_SESSION['user_id'] = $user_id;
            die("Login Successful");
        }else{
           die("Invalid email or password"); 
        }
    }
}else{
    //die("Not Set");
}

if(isset($_POST['register_email'])){
    if(isset($_POST['name'])){
    $name = $_POST['name'];
    if($name != '' && !ctype_space($name)){
        
      //$name = preg_replace('/[^a-zA-Z0-9_-]/','',$name);  
    }else{
        die('Kindly fill your name');
        
    }
    
}
if(isset($_POST['register_email'])){
    $email = $_POST['register_email'];
    if(!empty($email) && !ctype_space($email)){
        if(filter_var($email,FILTER_VALIDATE_EMAIL)){
      $email = filter_var($email,FILTER_SANITIZE_EMAIL);
    $sql = "SELECT id FROM user WHERE email = ?";
    $stmt = $db->prepare($sql);
    $stmt->execute([$email]);
    if($stmt->rowCount() > 0){
        die("This email is already registered");
    }
    }else{
        die('Invalid Email Address Provided');
        
    }
    }else{
        die('Email address is required');
    }
    
}
if(isset($_POST['phone'])){
    $phone = $_POST['phone'];
    if($phone != '' && !ctype_space($phone)){
      $phone = intval($phone);
        if(!$phone){
            die("Invalid Phone Number");
        }else{
            $sql = "SELECT id FROM user WHERE phone = ?";
    $stmt = $db->prepare($sql);
    $stmt->execute([$phone]);
    if($stmt->rowCount() > 0){
        die("This Phone Number is already registered");
    }
        }
    }else{
        die('Kindly fill your Phone Number');
        
    }
    
}
if(isset($_POST['password'])){
    $password = $_POST['password'];
}
   if(isset($_POST['password2'])){
       $password2 = $_POST['password2'];
   }
   
   if($password !== $password2){
       die("Passwords Do Not Match!");
   }else{
       $password = password_hash($password,PASSWORD_BCRYPT,array("cost" => 12));
   }
  $statement = "INSERT INTO user(name,email,phone,password) VALUES(?,?,?,?)";
   $stmt = $db->prepare($statement);
   if($stmt->execute([$name,$email,$phone,$password])){
       $user_id = $db->lastInsertId();
       $amount = my_encrypt(100000,$key);
       $sql_statement = "INSERT INTO pikash_account(user_id,amount) VALUES(?,?)";
       $stmt2 = $db->prepare($sql_statement);
       $stmt2->execute([$user_id,$amount]);
       $transaction_id = strtoupper(uniqid());
       $sql_statement = "INSERT INTO transactions(transaction_id,transaction_type,amount,sender,receiver,receiver_id,narration,balance_after) VALUES(?,?,?,?,?,?,?,?)";
       $stmt3 = $db->prepare($sql_statement);
       $stmt3->execute([$transaction_id,'credit',100000,$user_id,$phone,$user_id,'Initial Pikash Account Credit',100000]);
       $_SESSION['user_id'] = $user_id;
       echo "Registration Successful";
   }else{
       echo "System Error, Kindly Contact Administrator";
   }
   
   
}