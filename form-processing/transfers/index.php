<?php
ob_start();
session_start();
require_once "../../config/pik-dbase-config.php";
require_once "../../config/pik-functions.php";

$user_id = $_SESSION['user_id'];
$query = "SELECT * FROM user WHERE id = ?";
    $stmt = $db->prepare($query);
    if($stmt->execute([$user_id])){
    ##FETCH RECIPIENT DETAILS
while($row = $stmt->fetch(PDO::FETCH_OBJ)){
    $recipient_id = $row->id;
    $name = $row->name;
    $email = $row->email;
    $phone = $row->phone;
}    
        
    }
if(isset($_POST['account'])){
    $account = $_POST['account'];
}else{
    die();
}
if(isset($_POST['currency'])){
    $currency = $_POST['currency'];
}else{
    die();
}
if(isset($_POST['recipient'])){
    $recipient = $_POST['recipient'];
    if(!empty($recipient) && !ctype_space($recipient)){
        $recipient = intval($recipient);
    
    $query = "SELECT * FROM user WHERE phone = ?";
    $stmt = $db->prepare($query);
    if($stmt->execute([$recipient])){
        $count = $stmt->rowCount();
        if($count < 1){
            die("This Phone Number Is Not Registered With Pikash");
        }
    }
    }else{
        die("Please provide a recipient");
    }
}else{
    die();
}
if(isset($_POST['amount'])){
    $amount = $_POST['amount'];
    $amount = intval($amount);
    if($amount < 10){
        die("Minimum amount to transfer is KES 10.00");
    }
}else{
    die();
}

##FETCH RECIPIENT DETAILS
while($row = $stmt->fetch(PDO::FETCH_OBJ)){
    $recipient_id = $row->id;
    $username = $row->name;
    $email = $row->email;
    $phone = $row->phone;
}

##DEBIT SENDER ACCOUNT
$sql = "SELECT amount FROM pikash_account WHERE user_id = ?";
$stmt2 = $db->prepare($sql);
$stmt2->execute([$user_id]);
while($row2 = $stmt2->fetch(PDO::FETCH_OBJ)){
    $sender_amount = $row2->amount;
    
}

$sender_amount = my_decrypt($sender_amount,$key);

if($sender_amount > $amount){
    
}else{
    ##SENDER IS BROKE
    die("You do not have enough balance to make this transaction");
}
$new_amount = $sender_amount - $amount;
$new_amount_final = my_encrypt($new_amount,$key);
$sql3 = "UPDATE pikash_account SET amount = ? WHERE user_id = ?";
$stmt3 = $db->prepare($sql3);
if($stmt3->execute([$new_amount_final,$user_id])){
    $transaction_id = strtoupper(uniqid());
    $sql_statement = "INSERT INTO transactions(transaction_id,transaction_type,amount,sender,receiver,receiver_id,narration,balance_after) VALUES(?,?,?,?,?,?,?,?)";
       $stmt3 = $db->prepare($sql_statement);
       $stmt3->execute([$transaction_id,'debit',$amount,$user_id,$phone,$user_id,'Pikash Transfer to '.$username,$new_amount]);
     
}else{
  die("An Error Occured, Kindly Contact System Admin") ;
}


$sql = "SELECT amount FROM pikash_account WHERE user_id = ?";
$stmt2 = $db->prepare($sql);
$stmt2->execute([$recipient_id]);
while($row2 = $stmt2->fetch(PDO::FETCH_OBJ)){
    $recipient_amount = $row2->amount;
    
}

$recipient_amount = my_decrypt($recipient_amount,$key);
$new_amount2 = $recipient_amount + $amount;
$new_amount_final = my_encrypt($new_amount2,$key);
$sql3 = "UPDATE pikash_account SET amount = ? WHERE user_id = ?";
$stmt3 = $db->prepare($sql3);
if($stmt3->execute([$new_amount_final,$recipient_id])){
    $transaction_id = strtoupper(uniqid());
    $sql_statement = "INSERT INTO transactions(transaction_id,transaction_type,amount,sender,receiver,receiver_id,narration,balance_after) VALUES(?,?,?,?,?,?,?,?)";
       $stmt3 = $db->prepare($sql_statement);
       $stmt3->execute([$transaction_id,'credit',$amount,$user_id,$phone,$recipient_id,'Pikash Transfer from '.$name,$new_amount2]);
     echo "Transaction Successful, $username has been credited with Ksh: $amount . Your new account balance is $new_amount";
}else{
    
}



