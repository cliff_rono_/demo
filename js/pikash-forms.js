$(document).ready(function(){
    $('form').submit(function(e){
        e.preventDefault();
        var frm = $(this);


        $.ajax({
            type: frm.attr('method'),
            url: frm.attr('action'),
            data: frm.serialize(),
            success: function (response) {
                alert(response)
            },
            error: function (data) {
                console.log('An error occurred.');
                console.log(data);
            },
        });
    
    });
});