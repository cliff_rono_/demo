
      google.charts.load('current', {'packages':['bar']});
      google.charts.setOnLoadCallback(drawStuff);

      function drawStuff() {
        var data = new google.visualization.arrayToDataTable([
          ['Month', 'Transactions'],
          ["January", 44],
          ["February", 31],
          ["March", 12],
          ["April", 10],
          ['May', 3],
        ['June', 3],
        ['July', 200],
        ['August', 3],
        ['September', 200],
        ['October', 3],
        ['November', 200],
        ['December', 1500]
        ]);

        var options = {
            width: 1200,
          legend: { position: 'none' },
          chart: {
             },
          axes: {
            x: {
              0: { side: 'top'} // Top x-axis.
            }
          },
          bar: { groupWidth: "90%" }
        };

        var chart = new google.charts.Bar(document.getElementById('monthly_chart_div'));
        // Convert the Classic options to Material options.
        chart.draw(data, google.charts.Bar.convertOptions(options));
      };
