
      google.charts.load('current', {'packages':['bar']});
      google.charts.setOnLoadCallback(drawStuff);

      function drawStuff() {
        var data = new google.visualization.arrayToDataTable([
          ['Day', 'Transactions'],
          ["Monday", 44],
          ["Tuesday", 31],
          ["Wednesday", 12],
          ["Thursday", 10],
          ['Friday', 3],
            ['Saturday', 3],
            ['Sunday', 200]
        ]);

        var options = {
          legend: { position: 'none' },
          chart: {
             },
          axes: {
            x: {
              0: { side: 'top'} // Top x-axis.
            }
          },
          bar: { groupWidth: "90%" }
        };

        var chart = new google.charts.Bar(document.getElementById('weekly_chart_div'));
        // Convert the Classic options to Material options.
        chart.draw(data, google.charts.Bar.convertOptions(options));
      };
